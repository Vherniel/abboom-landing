const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg')
const mode = process.env.NODE_ENV || 'development';

module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  configureWebpack:{
    plugins: [
      new ImageminPlugin({
        plugins: [
          imageminMozjpeg({
            quality: 85
          })
        ]
      })
    ],
    devtool: (mode === 'development') ? 'inline-source-map' : false,
    optimization: {
      splitChunks: {
        minSize: 100000,
        maxSize: 250000,
      },
    }
  }
}

