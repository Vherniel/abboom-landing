import Vue from 'vue'
import VueI18n from 'vue-i18n'
import { syncGetData} from "@/apiUtil";
import NProgress from 'nprogress'
import './sass/nprogress.scss'

Vue.use(VueI18n)


let loadedLanguages = [];

export function initI18n() {
  const lang = navigator.language.split('-')[0] || process.env.VUE_APP_I18N_LOCALE;

  return syncGetData('/api/translations/' + lang)
    .then(function(data) {
      setI18nLanguage(data.locale)
      loadedLanguages.push(data.locale)
      return new VueI18n({
        locale: data.locale,
        fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE,
        messages: data
      })
    })
    .catch(function (error) {
      console.error(error)
    });
}

function setI18nLanguage (lang) {
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

export function loadLanguage(lang) {
  // If the language was already loaded
  if (loadedLanguages.includes(lang)) {
    return Promise.resolve(setI18nLanguage(lang))
  }
  NProgress.start();
  return syncGetData('/api/translations/' + lang)
    .then(function(data) {
      setI18nLanguage(lang)
      loadedLanguages.push(lang)
      return  data;
    })
    .catch(function (error) {
      console.error(error)
    }).finally(function () {
      NProgress.done();
    });
}


