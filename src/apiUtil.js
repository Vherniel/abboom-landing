export async function asyncPostData(uri = '', data = {}) {
  let url = process.env.VUE_APP_API_URL + uri;
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });

  return await handleErrors(response)
}

export function  syncGetData(uri = '') {
  let url = process.env.VUE_APP_API_URL + uri;
  // Default options are marked with *
  return fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(function (response) {
      return handleErrors(response)
    }
  );
}

export async function asyncGetData(uri = '') {
  let url = process.env.VUE_APP_API_URL + uri;
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })

  return await handleErrors(response)
}

export function handleErrors(response) {
  if (!response.ok) {
    return response.json() // return the result of the inner promise, which is an error
        .then((json) => {
          throw new Error(json.messages.join('~'));
        });
  }

  if (response.status == 204) return ;

  return response.json();
}
