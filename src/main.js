import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import './sass/styles.scss';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSignInAlt, faUserPlus, faSearchLocation} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import DatePicker from 'vue-md-date-picker'
import './sass/clock-picker.scss';
const VueClockPickerPlugin = require('@pencilpix/vue2-clock-picker/dist/vue2-clock-picker.plugin.js')
import VueSimpleSuggest from 'vue-simple-suggest'
import {initI18n} from './i18n'
import NProgress from "nprogress";
import './sass/nprogress.scss'
import vueDebounce from 'vue-debounce'

Vue.use(vueDebounce)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
library.add(faSignInAlt, faUserPlus, faSearchLocation)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('vue-simple-suggest', VueSimpleSuggest)


Vue.use(DatePicker)
Vue.component('md-date-picker', DatePicker)
Vue.use(VueClockPickerPlugin)

Vue.config.productionTip = false

NProgress.start();
initI18n().then(function(i18n) {
  new Vue({
    i18n,
    render: h => h(App)
  }).$mount('#app')
}).finally(function () {
  NProgress.done();
});
